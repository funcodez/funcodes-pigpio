package com.pi4j.catalog.components;

import java.util.logging.Logger;

public abstract class Component {

	private final Logger LOGGER = Logger.getLogger( getClass().getName() );

	protected void logInfo( String msg ) {
		LOGGER.info( () -> msg );
	}

	protected void logError( String msg ) {
		LOGGER.severe( () -> msg );
	}

	protected void logConfig( String msg ) {
		LOGGER.config( () -> msg );
	}

	protected void logDebug( String msg ) {
		LOGGER.fine( () -> msg );
	}

	/**
	 * Utility function to sleep for the specified amount of milliseconds. An
	 * {@link InterruptedException} will be caught and ignored while setting the
	 * interrupt flag again.
	 *
	 * @param milliseconds Time in milliseconds to sleep
	 */
	void delay( long milliseconds ) {
		try {
			Thread.sleep( milliseconds );
		}
		catch ( InterruptedException e ) {
			Thread.currentThread().interrupt();
		}
	}
}
