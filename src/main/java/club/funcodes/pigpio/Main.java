// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.pigpio;

import static org.refcodes.cli.CliSugar.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.FloatOption;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.Execution;
import org.refcodes.struct.Range;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

import com.github.mbelling.ws281x.Color;
import com.github.mbelling.ws281x.LedStripType;
import com.github.mbelling.ws281x.Ws281xLedStrip;
import com.pi4j.Pi4J;
import com.pi4j.catalog.components.LedStrip;
import com.pi4j.catalog.components.LedStrip.PixelColor;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalOutput;
import com.pi4j.io.gpio.digital.DigitalState;
import com.pi4j.io.gpio.digital.DigitalStateChangeEvent;
import com.pi4j.io.gpio.digital.DigitalStateChangeListener;
import com.pi4j.io.gpio.digital.PullResistance;
import com.pi4j.library.pigpio.PiGpio;
import com.pi4j.plugin.pigpio.provider.gpio.digital.PiGpioDigitalInputProvider;
import com.pi4j.plugin.pigpio.provider.gpio.digital.PiGpioDigitalOutputProvider;
import com.pi4j.plugin.pigpio.provider.spi.PiGpioSpiProvider;
import com.pi4j.plugin.raspberrypi.platform.RaspberryPiPlatform;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static enum Library {
		RPI, PI4J
	}

	private static final String NAME = "pigpio";
	private static final String TITLE = "PI⌁GPIO";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/pigpio_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "A command line tool for testing a simple Raspberry Pi setup with three LEDs (R,G,B) and a push button (see [https://www.metacodes.pro/manpages/pigpio_manpage]).";

	private static final String TRAFFIC_LIGHT_PROPERTY = "trafficLights";
	private static final String LED_STRIP_PROPERTY = null;

	private static final String STEP_PROPERTY = "step";
	private static final String LEDS_PROPERTY = "leds";
	private static final String LEDS_DELIMITER = ",";

	private static final String DATA_IN_SPI_CHANNEL_PROPERTY = "spi/dataIn";
	private static final String GREEN_LED_PIN_PROPERTY = "pins/green";
	private static final String YELLOW_LED_PIN_PROPERTY = "pins/yellow";
	private static final String RED_LED_PIN_PROPERTY = "pins/red";
	private static final String BUTTON_PIN_PROPERTY = "pins/button";
	private static final String PULL_RESISTANCE_PROPERTY = "pins/button/pullResistance";
	private static final String STRIP_LENGTH_PROPERTY = "strip/length";
	private static final String STRIP_BRIGHTNESS_PROPERTY = "strip/brightness";
	private static final String STRIP_LIBRARY_PROPERTY = "strip/library";

	private static final int DEFAULT_DATA_IN_SPI_CHANNEL = 0;
	private static final Library DEFAULT_STRIP_LIBRARY = Library.PI4J;
	private static final float DEFAULT_STRIP_BRIGHTNESS = 1F;
	private static final int DEFAULT_STRIP_LENGTH = 16;
	private static final int DEFAULT_GREEN_LED_PIN = 3;
	private static final int DEFAULT_YELLOW_LED_PIN = 4;
	private static final int DEFAULT_RED_LED_PIN = 5;
	private static final int DEFAULT_BUTTON_PIN = 6;
	private static final PullResistance DEFAULT_PULL_RESISTANCE_PROPERTY = PullResistance.PULL_DOWN;

	private static final int SPI0_MOSI_GPIO_PIN = 10;
	private static final int SPI1_MOSI_GPIO_PIN = 20;
	private static final int RPI_DMA = 10;
	private static final int RPI_FREQUENCY = 800000;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final Flag theTrafficLightFlag = flag( 't', "traffic-lights", TRAFFIC_LIGHT_PROPERTY, "Go for the traffic lights test!" );
		final IntOption theGreenLedPinArg = intOption( 'g', "green", GREEN_LED_PIN_PROPERTY, "The pin number having the green LED connected." );
		final IntOption theYellowLedPinArg = intOption( 'y', "yellow", YELLOW_LED_PIN_PROPERTY, "The pin number having the yellow LED connected." );
		final IntOption theRedLedPinArg = intOption( 'r', "red", RED_LED_PIN_PROPERTY, "The pin number having the red LED connected." );
		final IntOption theButtonPinArg = intOption( 'b', "button", BUTTON_PIN_PROPERTY, "The pin number having the push button connected." );
		final EnumOption<PullResistance> thePullResistanceArg = enumOption( 'p', "pull", PullResistance.class, PULL_RESISTANCE_PROPERTY, "The pull resistance to be used for the button: " + VerboseTextBuilder.asString( PullResistance.values() ) );
		final EnumOption<Library> theLibArg = enumOption( "library", Library.class, STRIP_LIBRARY_PROPERTY, "The library to use: " + VerboseTextBuilder.asString( Library.values() ) );

		final Flag theLedStripFlag = flag( 'l', "led-strip", LED_STRIP_PROPERTY, "Go for the led strip test!" );
		final IntOption theSpiChannelArg = intOption( 'c', "channel", DATA_IN_SPI_CHANNEL_PROPERTY, "The SPI channel having the LED stripe connected to." );
		final Flag theStepFlag = flag( 's', "step", STEP_PROPERTY, "Forces the user to hit <ENTER> when testing the individual LEDs one after the other." );
		final StringOption theLedsArg = stringOption( "leds", LEDS_PROPERTY, "The LED stripe's LEDs to be tests (comma separated values and '-' denoted ranges." );
		final IntOption theStripLengthArg = intOption( "length", STRIP_LENGTH_PROPERTY, "The lengt (number of LEDs) of the LED strip." );
		final FloatOption theBrightnessArg = floatOption( "brightness", STRIP_BRIGHTNESS_PROPERTY, "The brightness (0.0 - 1.0) of the LED stips's LEDs." );

		final Flag theInitFlag = initFlag( false );
		final Flag theDebugFlag = debugFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final ConfigOption theConfigArg = configOption();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( theTrafficLightFlag,  any( theGreenLedPinArg, theYellowLedPinArg, theRedLedPinArg, theButtonPinArg, thePullResistanceArg, theConfigArg, theVerboseFlag, theDebugFlag ) ),
			and( theLedStripFlag, any ( theLedsArg, theStripLengthArg, theBrightnessArg, theSpiChannelArg, theStepFlag, theLibArg, theConfigArg, theVerboseFlag, theDebugFlag ) ),
			and( theInitFlag, optional( theConfigArg, theVerboseFlag ) ),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Test traffic lights with defaults", theVerboseFlag), 
			example( "Test traffic lights with settings", theRedLedPinArg, theGreenLedPinArg, theYellowLedPinArg, theButtonPinArg, theVerboseFlag),
			example( "Test traffic lights using config file", theConfigArg, theVerboseFlag),
			example( "Test LED strip", theLedStripFlag, theVerboseFlag ),
			example( "Test LED strip with dedicated library", theLedStripFlag, theLibArg, theVerboseFlag ),
			example( "Test LED strip with brightness", theLedStripFlag, theBrightnessArg, theVerboseFlag ),
			example( "Test LED strip for LEDs", theLedStripFlag, theLedsArg, theVerboseFlag ),
			example( "Test LED strip stepwise for LEDs", theLedStripFlag, theLedsArg, theStepFlag, theVerboseFlag ),
			example( "Test LED strip stepwise on channel for LEDs", theLedStripFlag, theLedsArg, theStepFlag, theSpiChannelArg, theVerboseFlag ),
			example( "Initialize default config file", theInitFlag ),
			example( "Initialize specific config file", theInitFlag, theConfigArg),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theArgsProperties.getBoolean( theVerboseFlag );
		final boolean isTrafficLights = theArgsProperties.getBoolean( theTrafficLightFlag );
		final boolean isLedStrip = theArgsProperties.getBoolean( theLedStripFlag );

		try {
			if ( isTrafficLights ) {
				int theGreenPin = theArgsProperties.getIntOr( theGreenLedPinArg, DEFAULT_GREEN_LED_PIN );
				int theYellowPin = theArgsProperties.getIntOr( theYellowLedPinArg, DEFAULT_YELLOW_LED_PIN );
				int theRedPin = theArgsProperties.getIntOr( theRedLedPinArg, DEFAULT_RED_LED_PIN );
				int theButtonPin = theArgsProperties.getIntOr( theButtonPinArg, DEFAULT_BUTTON_PIN );
				PullResistance thePullResistance = theArgsProperties.getEnumOr( thePullResistanceArg, DEFAULT_PULL_RESISTANCE_PROPERTY );
				testTrafficLights( theGreenPin, theYellowPin, theRedPin, theButtonPin, thePullResistance, isVerbose );
			}
			if ( isLedStrip ) {
				Float theBrightness = theArgsProperties.getFloatOr( theBrightnessArg, DEFAULT_STRIP_BRIGHTNESS );
				Integer theStripLength = theArgsProperties.getIntOr( theStripLengthArg, DEFAULT_STRIP_LENGTH );
				String theLedsExpression = theArgsProperties.getOr( theLedsArg, "0-" + ( theStripLength - 1 ) );
				Integer theSpiChannel = theArgsProperties.getIntOr( theSpiChannelArg, DEFAULT_DATA_IN_SPI_CHANNEL );
				Boolean isStep = theArgsProperties.getBoolean( theStepFlag );
				Library theLib = theArgsProperties.getEnumOr( theLibArg, DEFAULT_STRIP_LIBRARY );
				// testLedStripRpiWs281x( theLedsExpression, theBrightness, theStripLength, theSpiChannel, isStep, isVerbose );
				testLedStrip( theLedsExpression, theBrightness, theStripLength, theSpiChannel, isStep, isVerbose, theLib );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	private static void testTrafficLights( int aGreenPin, int aYellowPin, int aRedPin, int aButtonPin, PullResistance aPullResistance, boolean isVerbose ) throws IOException, InterruptedException {
		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Using pin <" + aGreenPin + "> for green LED ..." );
			LOGGER.info( "Using pin <" + aYellowPin + "> for yellow LED ..." );
			LOGGER.info( "Using pin <" + aRedPin + "> for red LED ..." );
			LOGGER.info( "Using pin <" + aButtonPin + "> for push button ..." );
			LOGGER.info( "Using pull resistance <" + aPullResistance + "> for button ..." );
		}

		final var thePi4j = Pi4J.newAutoContext();
		Execution.addShutdownHook( new Thread( () -> thePi4j.shutdown() ) );
		final DigitalOutput theGreenPin = thePi4j.dout().create( DigitalOutput.newConfigBuilder( thePi4j ).address( aGreenPin ).name( "GreenLED" ).initial( DigitalState.LOW ).shutdown( DigitalState.LOW ).build() );
		final DigitalOutput theYellowPin = thePi4j.dout().create( DigitalOutput.newConfigBuilder( thePi4j ).address( aYellowPin ).name( "YellowLED" ).initial( DigitalState.LOW ).shutdown( DigitalState.LOW ).build() );
		final DigitalOutput theRedPin = thePi4j.dout().create( DigitalOutput.newConfigBuilder( thePi4j ).address( aRedPin ).name( "RedLED" ).initial( DigitalState.LOW ).shutdown( DigitalState.LOW ).build() );
		final DigitalInput theButtonPin = thePi4j.din().create( DigitalInput.newConfigBuilder( thePi4j ).address( aButtonPin ).name( "PushButton" ).pull( aPullResistance ).build() );

		doFlipLights( theGreenPin, theYellowPin, theRedPin, 3000, isVerbose );

		{
			if ( isVerbose ) {
				LOGGER.printSeparator();
				LOGGER.info( "Waiting for button push ..." );
			}
			theButtonPin.addListener( new DigitalStateChangeListener() {

				@Override
				@SuppressWarnings("rawtypes")
				public void onDigitalStateChange( DigitalStateChangeEvent aEvent ) {
					doFlipLights( theGreenPin, theYellowPin, theRedPin, 500, isVerbose );
				}
			} );
		}
		if ( System.console() != null && System.console().reader() != null && isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Hit <Strg+C> to abort, <Enter> flips LEDS ..." );
		}
		if ( System.console() != null && System.console().reader() != null ) {
			while ( true ) {
				System.console().reader().read();
				doFlipLights( theGreenPin, theYellowPin, theRedPin, 500, isVerbose );
			}
		}
		synchronized ( Main.class ) {
			Main.class.wait();
		}
		thePi4j.shutdown();
	}

	private static void doFlipLights( DigitalOutput aGreenPin, DigitalOutput aYellowPin, DigitalOutput aRedPin, int aFlipTimeMs, boolean isVerbose ) {
		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Flip lights delay is <" + aFlipTimeMs + "> ms ..." );
			LOGGER.printSeparator();
		}

		aGreenPin.high();
		if ( isVerbose ) {
			LOGGER.info( "GREEN switched <ON> ..." );
		}
		try {
			Thread.sleep( aFlipTimeMs );
		}
		catch ( InterruptedException ignore ) {}

		aYellowPin.high();
		if ( isVerbose ) {
			LOGGER.info( "YELLOW switched <ON> ..." );
		}
		try {
			Thread.sleep( aFlipTimeMs );
		}
		catch ( InterruptedException ignore ) {}

		aRedPin.high();
		if ( isVerbose ) {
			LOGGER.info( "RED switched <ON> ..." );
		}
		try {
			Thread.sleep( aFlipTimeMs );
		}
		catch ( InterruptedException ignore ) {}

		aGreenPin.low();
		if ( isVerbose ) {
			LOGGER.info( "GREEN turned <OFF>!" );
		}
		try {
			Thread.sleep( aFlipTimeMs );
		}
		catch ( InterruptedException ignore ) {}

		aYellowPin.low();
		if ( isVerbose ) {
			LOGGER.info( "YELLOW turned <OFF>!" );
		}
		try {
			Thread.sleep( aFlipTimeMs );
		}
		catch ( InterruptedException ignore ) {}

		aRedPin.low();
		if ( isVerbose ) {
			LOGGER.info( "RED turned <OFF>!" );
		}
	}

	private static void testLedStrip( String aLedsExpression, Float aBrightness, Integer aStripLength, Integer aSpiChannel, Boolean isStep, boolean isVerbose, Library aLib ) throws IOException, InterruptedException {
		switch ( aLib ) {
		case PI4J:
			testLedStripPi4J( aLedsExpression, aBrightness, aStripLength, aSpiChannel, isStep, isVerbose );
			break;
		case RPI:
			testLedStripRpiWs281x( aLedsExpression, aBrightness, aStripLength, aSpiChannel, isStep, isVerbose );
		}
	}

	private static void testLedStripRpiWs281x( String aLedsExpression, Float aBrightness, int aStripLength, int aSpiChannel, boolean isStepEnabled, boolean isVerbose ) throws IOException, InterruptedException {
		final List<Integer> theLeds = toLeds( aLedsExpression, aStripLength );
		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "LED strip configured with <" + aStripLength + "> LEDs ..." );
			LOGGER.info( "Using SPI channel <" + aSpiChannel + "> to drive the LED stripe ..." );
			LOGGER.info( "Testing LEDs <" + VerboseTextBuilder.asString( theLeds ) + ">  ..." );
			LOGGER.info( "Setting LEDs brightness to <" + aBrightness + ">  ..." );
			LOGGER.info( "Hit <ENTER> to step is <" + ( isStepEnabled ? "ENABLED" : "DISABLED" ) + ">  ..." );
		}

		final int theSpiGpioPin = aSpiChannel == 0 ? SPI0_MOSI_GPIO_PIN : ( aSpiChannel == 1 ? SPI1_MOSI_GPIO_PIN : SPI0_MOSI_GPIO_PIN );
		final int theBrightness = (int) ( aBrightness * 255F );
		final Ws281xLedStrip theLedStrip = new Ws281xLedStrip( aStripLength, theSpiGpioPin, RPI_FREQUENCY, RPI_DMA, theBrightness, 0, false, LedStripType.WS2811_STRIP_GRB, false );

		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Clearing all LEDs ..." );
		}

		theLedStrip.setStrip( Color.BLACK );
		theLedStrip.render();
		Thread.sleep( 1500 );

		if ( isVerbose ) {
			if ( isStepEnabled ) {
				LOGGER.printTail();
			}
			else {
				LOGGER.printSeparator();
			}
		}
		Scanner theScanner = null;
		if ( isStepEnabled ) {
			theScanner = new Scanner( System.in );
		}
		for ( int eLed : theLeds ) {
			if ( isStepEnabled ) {
				System.out.print( "Hit <ENTER> to test LED <" + eLed + "> ..." );
				theScanner.nextLine();
			}
			else {
				if ( isVerbose ) {
					LOGGER.info( "LED <" + eLed + "> now is on!" );
				}
			}
			theLedStrip.setPixel( eLed, Color.WHITE );
			theLedStrip.render();
			if ( !isStepEnabled ) {
				Thread.sleep( 250 );
			}
		}

		if ( isStepEnabled ) {
			System.out.print( "Hit <ENTER> to abort ..." );
			theScanner.nextLine();
		}
		else {
			if ( isVerbose ) {
				LOGGER.info( "Hit <Strg+C> to abort ..." );
			}
			synchronized ( Main.class ) {
				Main.class.wait();
			}
		}
	}

	private static void testLedStripPi4J( String aLedsExpression, Float aBrightness, int aStripLength, int aSpiChannel, boolean isStepEnabled, boolean isVerbose ) throws IOException, InterruptedException {

		final List<Integer> theLeds = toLeds( aLedsExpression, aStripLength );

		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "LED strip configured with <" + aStripLength + "> LEDs ..." );
			LOGGER.info( "Using SPI channel <" + aSpiChannel + "> to drive the LED stripe ..." );
			LOGGER.info( "Testing LEDs <" + VerboseTextBuilder.asString( theLeds ) + ">  ..." );
			LOGGER.info( "Setting LEDs brightness to <" + aBrightness + ">  ..." );
			LOGGER.info( "Hit <ENTER> to step is <" + ( isStepEnabled ? "ENABLED" : "DISABLED" ) + ">  ..." );
		}

		// ---------------------------------------------------------------------
		// final var thePiGpio = PiGpio.newNativeInstance();
		// @formatter:off
		//	final var thePi4j = Pi4J.newContextBuilder().noAutoDetect().add(
		//		PiGpioDigitalInputProvider.newInstance(thePiGpio),
		//        PiGpioDigitalOutputProvider.newInstance(thePiGpio),
		//        PiGpioPwmProvider.newInstance(thePiGpio),
		//        PiGpioI2CProvider.newInstance(thePiGpio),
		//        PiGpioSerialProvider.newInstance(thePiGpio),
		//        PiGpioSpiProvider.newInstance(thePiGpio)
		//	).build();
		// @formatter:on
		// ---------------------------------------------------------------------
		final var thePiGpio = PiGpio.newNativeInstance();
		final var thePi4j = Pi4J.newContextBuilder().noAutoDetect().add( new RaspberryPiPlatform() {
			@Override
			protected String[] getProviders() {
				return new String[] {};
			}
		} ).add( PiGpioDigitalInputProvider.newInstance( thePiGpio ), PiGpioDigitalOutputProvider.newInstance( thePiGpio ), PiGpioSpiProvider.newInstance( thePiGpio ) ).build();
		// ---------------------------------------------------------------------
		// final var thePi4j = Pi4J.newAutoContext();
		// ---------------------------------------------------------------------

		final var theLedStrip = new LedStrip( thePi4j, aStripLength, aBrightness, aSpiChannel );
		Execution.addShutdownHook( new Thread( () -> { theLedStrip.close(); thePi4j.shutdown(); } ) );

		if ( isVerbose ) {
			LOGGER.printSeparator();
			LOGGER.info( "Clearing all LEDs ..." );
		}

		theLedStrip.allOff();
		theLedStrip.render();
		Thread.sleep( 1500 );

		if ( isVerbose ) {
			if ( isStepEnabled ) {
				LOGGER.printTail();
			}
			else {
				LOGGER.printSeparator();
			}
		}
		Scanner theScanner = null;
		if ( isStepEnabled ) {
			theScanner = new Scanner( System.in );
		}
		for ( int eLed : theLeds ) {
			if ( isStepEnabled ) {
				System.out.print( "Hit <ENTER> to test LED <" + eLed + "> ..." );
				theScanner.nextLine();
			}
			else {
				if ( isVerbose ) {
					LOGGER.info( "LED <" + eLed + "> now is on!" );
				}
			}
			theLedStrip.setPixelColor( eLed, PixelColor.WHITE );
			theLedStrip.render();
			if ( !isStepEnabled ) {
				Thread.sleep( 250 );
			}
		}

		if ( isStepEnabled ) {
			System.out.print( "Hit <ENTER> to abort ..." );
			theScanner.nextLine();
		}
		else {
			if ( isVerbose ) {
				LOGGER.info( "Hit <Strg+C> to abort ..." );
			}
			synchronized ( Main.class ) {
				Main.class.wait();
			}
		}
	}

	private static List<Integer> toLeds( String aLedsExpression, int aStripLength ) {
		final StringTokenizer theTokens = new StringTokenizer( aLedsExpression, LEDS_DELIMITER );
		final List<Integer> theLeds = new ArrayList<>();
		String eToken;
		Range<Integer> eRange;
		while ( theTokens.hasMoreTokens() ) {
			eToken = theTokens.nextToken().trim();
			if ( eToken.indexOf( '-' ) != -1 ) {
				eRange = Range.toRange( eToken, Integer.class );
				for ( int i = eRange.getMinValue(); i < eRange.getMaxValue() + 1; i++ ) {
					if ( i < 0 || i > aStripLength - 1 ) {
						throw new IndexOutOfBoundsException( "The LED index <" + i + "> must be between <0> and <" + ( aStripLength - 1 ) + "> !" );
					}
					theLeds.add( i );
				}
			}
			else {
				Integer eValue = Integer.valueOf( eToken );
				if ( eValue < 0 || eValue > aStripLength - 1 ) {
					throw new IndexOutOfBoundsException( "The LED index <" + eValue + "> must be between <0> and <" + ( aStripLength - 1 ) + "> !" );
				}
				theLeds.add( eValue );
			}
		}
		if ( theLeds.size() == 0 ) {
			throw new IllegalArgumentException( "The LEDs argument must not be something empty (e.g. whitespace only?)!" );
		}
		return theLeds;
	}
}
